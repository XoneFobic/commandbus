<?php namespace XoneFobic\CommandBus\Exceptions;

use Exception;

/**
 * Class CommandHandlerException
 *
 * @package XoneFobic\CommandBus\Exceptions
 */
class CommandHandlerException extends Exception {

}
