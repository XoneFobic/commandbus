<?php namespace XoneFobic\CommandBus;

use Illuminate\Foundation\Application;
use XoneFobic\CommandBus\Interfaces\CommandBusInterface;
use XoneFobic\CommandBus\Translators\CommandTranslator;

/**
 * Class ValidationCommandBus
 *
 * @package XoneFobic\CommandBus
 */
class ValidationCommandBus implements CommandBusInterface {

    /**
     * @var BaseCommandBus
     */
    private $commandBus;

    /**
     * @var Application
     */
    private $app;

    /**
     * @var Translators\CommandTranslator
     */
    private $commandTranslator;

    /**
     * @param BaseCommandBus    $commandBus
     * @param Application       $app
     * @param CommandTranslator $commandTranslator
     */
    function __construct(BaseCommandBus $commandBus, Application $app, CommandTranslator $commandTranslator)
    {
        $this->commandBus        = $commandBus;
        $this->app               = $app;
        $this->commandTranslator = $commandTranslator;
    }

    /**
     * @param $command
     *
     * @return mixed|void
     */
    public function execute($command)
    {
        $validator = $this->commandTranslator->toValidator($command);

        if (class_exists($validator))
        {
            $this->app->make($validator)->validate($command);
        }

        $this->commandBus->execute($command);
    }

}
