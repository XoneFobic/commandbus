<?php namespace XoneFobic\CommandBus\Interfaces;

/**
 * Interface CommandHandlerInterface
 *
 * @package XoneFobic\CommandBus\Interfaces
 */
interface CommandHandlerInterface {

    /**
     * @param $command
     *
     * @return mixed
     */
    public function handle($command);

} 
