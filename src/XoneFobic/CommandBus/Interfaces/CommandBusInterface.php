<?php namespace XoneFobic\CommandBus\Interfaces;

/**
 * Interface CommandBusInterface
 *
 * @package XoneFobic\CommandBus\Interfaces
 */
interface CommandBusInterface {

    /**
     * @param $command
     *
     * @return mixed
     */
    public function execute($command);

}
