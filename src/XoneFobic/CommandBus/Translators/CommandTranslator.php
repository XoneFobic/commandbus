<?php namespace XoneFobic\CommandBus\Translators;

use XoneFobic\CommandBus\Exceptions\CommandHandlerException;

/**
 * Class CommandTranslator
 *
 * @package XoneFobic\CommandBus\Translators
 */
class CommandTranslator {

    /**
     * Rename given className to convention and enforce it exists
     *
     * @param $command
     *
     * @return mixed
     * @throws \XoneFobic\CommandBus\Exceptions\CommandHandlerException
     */
    public function toCommandHandler($command)
    {
        $handler = str_replace('Command', 'CommandHandler', get_class($command));

        if ( ! class_exists($handler))
        {
            $message = "Command handler [$handler] does not exist.";

            throw new CommandHandlerException($message);
        }

        return $handler;
    }

    /**
     * Rename given className to convention
     *
     * @param $command
     *
     * @return mixed
     */
    public function toValidator($command)
    {
        return str_replace('Command', 'Validator', get_class($command));
    }

} 
