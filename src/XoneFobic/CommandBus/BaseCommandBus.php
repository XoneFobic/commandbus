<?php namespace XoneFobic\CommandBus;

use Illuminate\Foundation\Application;
use XoneFobic\CommandBus\Interfaces\CommandBusInterface;

/**
 * Class BaseCommandBus
 *
 * @package XoneFobic\CommandBus
 */
class BaseCommandBus implements CommandBusInterface {

    /**
     * @var \Illuminate\Foundation\Application
     */
    private $app;

    /**
     * @var CommandTranslator
     */
    private $commandTranslator;

    /**
     * @param Application       $app
     * @param CommandTranslator $commandTranslator
     */
    function __construct(Application $app, CommandTranslator $commandTranslator)
    {
        $this->app               = $app;
        $this->commandTranslator = $commandTranslator;
    }

    /**
     * @param $command
     *
     * @return mixed
     */
    public function execute($command)
    {
        $handler = $this->commandTranslator->toCommandHandler($command);

        return $this->app->make($handler)->handle($command);
    }

} 
